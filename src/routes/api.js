import mongoose from 'mongoose';
import userCont from '../controllers/userController.js'
import express from 'express'
import dotenv from 'dotenv' 
import adminCont from '../admin/adminController.js' 
import adminAuth from '../middleware/auth.js'
import orderRoutes from '../controllers/orderController.js'

dotenv.config()


const router = express.Router()
mongoose.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@kaicluster.zfpls.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`,
{useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log('Connected')
});


// Admin
router.post('/admin/', adminCont.adminLogin),
router.get('/admin/', adminCont.logout)
router.get('/admin/users/', adminAuth, adminCont.allUsers);
router.get('/admin/users/:_id/', adminAuth, adminCont.oneUser)
router.patch('/admin/users/:_id/update/', adminAuth, adminCont.updateUser)
router.post('/admin/products/add/', adminAuth, adminCont.addProduct)
router.get('/admin/products/', adminAuth, adminCont.allProducts)
router.get('/admin/products/:_id/', adminAuth, adminCont.getSingleProduct)
router.post('/admin/category/add/', adminAuth, adminCont.addCategory)
router.patch('/admin/products/:_id/update/', adminAuth, adminCont.singleProductUpdate)
router.delete('/admin/products/:_id/delete/', adminAuth, adminCont.removeProduct)
router.get('/orders/', adminAuth, adminCont.getAllOrders)


// router.post('/admin/createrole/', auth, adminCont.createRole),
// router.get('/admin/roles/', auth, adminCont.allRoles)

// users
router.post('/users/register', userCont.registerUser)
router.post('/users/login/', userCont.loginUser)
router.get('/users/logout/', userCont.logout)
router.get('/products/', adminCont.allProducts)
router.post('/orders/create/', orderRoutes.createOrder)




export default router