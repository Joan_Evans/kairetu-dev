import {orderSchema, productSchema} from '../models/product.js'
import mongoose from 'mongoose';
import modelExports from '../models/usersModel.js';
import cryptorandomstring from 'crypto-random-string' 
import cryptoRandomString from 'crypto-random-string';
//
const ref_number = cryptoRandomString({
    length:12,
    type:'alphanumeric'
})
const customer_id = cryptoRandomString({
    length:7,
    type: 'alphanumeric'

})

// models
const Product = mongoose.model('Products', productSchema)
const Order  = mongoose.model('Orders', orderSchema)
const User = mongoose.model('Users', modelExports.userSchema)

const createOrder = (req, res) => {
    let id = req.user == undefined ? customer_id : req.user.id
    const order = new Order({        
        reference_number: ref_number,
        user_id: id,
        items: req.body.items,
        total: req.body.total    

    });
    order.save((err, order) => {
        if (err) return res.status(422).send(err)
        res.cookie('orderToken', {ref_num: order.reference_number}, {maxAge: 900000, httpOnly: true} )
        res.status(201).json({
            message: 'order created',
            order: order
        });

    });

}
const updateOrder = (req, res) => {
    let {reference_number, user_id} = req.order
}
const orderRoutes = {
    'createOrder': createOrder,
    'updateOrder': updateOrder,
    'updateOrder': updateOrder
}
export default orderRoutes



