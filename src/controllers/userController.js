import mongoose from 'mongoose'
import modelExports from '../models/usersModel.js';
import cryptoRandomString from 'crypto-random-string' // generate secretkey
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import dotenv from 'dotenv' 
dotenv.config()



const secretKey = process.env.SECRET_KEY


// models
const User = mongoose.model('Users', modelExports.userSchema)
const Payment = mongoose.model('Payment', modelExports.paymentSchema)


const hashPassword = (password, hashed) =>{
    bcrypt.genSalt(15, (err, salt) => {
        if (err) {
            return err
        }
        bcrypt.hash(password, salt, (err, hash) => {
            if (err) {
                return err
            }
            hashed(hash)
        });
    });
};

const registerUser = (req, res) => {
    let userData = req.body
    if (!userData.email && !userData.name && !!userData.password){ // BUG this condition is not being checked
        return res.status(400).send('Data not formatted correctly')
    }
    User.findOne({ email: userData.email }, (err, retrievedUser ) => {
        if (err) {
            return res.status(422).send('Something went wrong')
        }
        if (retrievedUser){
            return res.status(422).send('User already exists')
        } else {
            let user = new User(userData) 
            hashPassword(userData.password, (hash) => {
                user.password = hash    
                user.save((err, regUser) => {
                    if (err) {
                        return res.status(422).send('Something went wrong while registering')
                    } else {
                        let payload = { id: regUser._id, email: regUser.email}
                        let token = jwt.sign(payload, secretKey)
                        res.cookie('nToken', token, { maxAge: 900000, httpOnly: true })
                        res.status(200).send({token})
                    }           
                })         
            });
        }          
    });
};

const loginUser = (req, res) => {
    let userInfo = req.body

    User.findOne({email: userInfo.email}, (err, user) => {
        if (err) {
            return res.status(400).send('User not found')
        } 
        if (!user) {
            return res.status(400).send('Register')

        } else {
            console.log(userInfo.password)
            user.matchPassword(userInfo.password, (err, match) => {
                if (err) return res.status(400).json({'err':err})
                if (!match) {
                    return res.status(400).send('Invalid password')
                }else {
                    let payload = {email: user.email, id:user._id }  
                    let token = jwt.sign(payload, secretKey, { expiresIn: "1d" }) // TODO read on jwt parameters expires on
                    res.cookie('nToken', token, { maxAge: 900000, httpOnly: true })
                    res.status(200).send({ token })
                }
            }); 
        }
    })
}
const logout = (req, res) => {
    res.clearCookie('nToken');
    res.status('200').send('Logged out');
}
let userCont = {
    'registerUser': registerUser,    
    'loginUser': loginUser,
    'logout': logout
}
      


export default userCont
