import express from 'express'
import bodyParser from 'body-parser'     
import api from './routes/api.js'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import dotenv from 'dotenv' 
dotenv.config()


const PORT = process.env.PORT
const app = express()
app.use(bodyParser.json())
app.use(morgan('tiny'))
app.use(cookieParser())
app.get('/', (req, res) =>{
    res.send('server')
})
app.use('/api',api)

app.listen(PORT, () => {
    console.log(`server running on ${PORT}`)
})
