import jwt from 'jsonwebtoken'
import cryptoRandomString from 'crypto-random-string'
import dotenv from 'dotenv' 
dotenv.config()

const secretKey = process.env.SECRET_KEY
const adminAuth = (req, res, next) => {
    if (req.headers.authorization == undefined) {
        return res.status(422).send('Authentication needed')
    } else {
        const token = req.headers.authorization.split(' ')[1];
        jwt.verify(token, secretKey, (err, decoded) => {
            if (err) return res.status(400).json({'error': err})
    
            if (req.body.email && req.body.email !== decoded.email) {
                res.status(400).send('Not authorised')
            } else {
                console.log(decoded)
                req.user = decoded
                next();
            }
          
        });
    }
    
} // TODO set up authentication

const userAuth = (req, res, next) => {
    if (typeof req.cookies.ntoken == "undefined" || req.cookies.ntoken === null) {
        return res.status(422).send('Authentication needed')
    } else {
        const ntoken = req.cookies.nToken 
        const token = ntoken.split(' ')[1];
        jwt.verify(token, secretKey, (err, user) => {
            if (err) return res.status(400).json({'error': err})
    
            if (req.body.email && req.body.email !== user.email) {
                res.status(400).send('Not authorised')
            } else {
                console.log(user)
                req.user = user
                next();
            }
        })

    }
}
const orderAuth = (req, res, next) => {
    if (typeof req.cookies.orderToken == undefined || req.cookies.orderToken === null){

    } else {
        const cookie = req.cookies.orderToken
        console.log(cookie)
        req.order = cookie
        next();
    }
}
export default adminAuth