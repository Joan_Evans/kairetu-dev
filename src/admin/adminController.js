import mongoose from 'mongoose'
import modelExports from '../models/usersModel.js';

import {productSchema, CategorySchema, orderSchema} from '../models/product.js'
import cryptoRandomString from 'crypto-random-string' // generate secretkey
import multer from 'multer'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv' 

dotenv.config()

// configure multer
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname)
    }
});
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

// cloudinary

import cloudinary from 'cloudinary'
cloudinary.v2.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
})

const secretKey = process.env.SECRET_KEY

// models
const User = mongoose.model('Users', modelExports.userSchema)
const Product = mongoose.model('Products', productSchema)
const Category = mongoose.model('Category', CategorySchema)
const Order = mongoose.model('Orders', orderSchema)

const adminLogin = (req, res) => {
    let userInfo = req.body

    User.findOne({email: userInfo.email}, (err, user) => {
        if (err) {
            return res.status(400).send(err)
        } 
        if (!user) {
            return res.status(400).send('Register')
        } else {
            user.matchPassword(userInfo.password, (err, match) => {
                if (err) return res.status(400).json({'err':err})
                if (!match) {
                    return res.status(400).send('Invalid password')
                }else {
                    let payload = {email: user.email, id:user._id, role: user.role }  
                    let token = jwt.sign(payload, secretKey, { expiresIn: "1d" }) // TODO read on jwt parameters expires on
                    res.cookie('nToken', token, { maxAge: 900000, httpOnly: true })
                    res.status(200).send({ token })
                }
            })                 
        }
    })
}
const allUsers = (req, res) => {
    const { role } = req.user
    if (role !== 'admin'){
        return res.status(403).send('Forbidden')
    } else {
        User.find((err, users) => {
            if (err) {
                return res.status(422).send('Could not fetch all users')
            } else {
                return res.json(users)
            }
        });
    }
    
}

const getOneUser = (req, res) => {
    const { role } = req.user
    if (role !== 'admin'){
        return res.status(403).send('Forbidden')
    } else {
        User.findById(req.params._id, (err, user) => {
            if (err) {
                return res.status(422).send('User not found')
            } else {
                return res.status(200).send(user)
            }
        });
    }
    
}

const updateOneUser = (req, res) => {
    let id = req.params._id
    let updateData = req.body
    const { role } = req.user
    if (role !== 'admin'){
        return res.status(403).send('Forbidden')
    } else {
        User.findByIdAndUpdate(id, {$set: updateData}, (err, doc) => {
            if (err) {
                return res.status(422).send(err)
            } else {
                return res.status(200).send(doc)
            }
        });
    }    
}
const addCategory = (req, res) => {
    const { role } = req.user
    if (role !== 'admin') {

    }else { 
        let CategoryData = req.body
        let category = new Category(CategoryData)
        category.save((err, cat) => {
            if (err) return res.status(400).send('Something went wrong')
            res.status(201).json({message: 'Category added',
                                category: cat
                                });
        });
    }
}

// Products
const addProduct =  (req, res) => {
    const upload = multer({
        storage: storage, 
        limits: {
            fileSize: 1024 * 1024 * 5
        },
        fileFilter: fileFilter
    }).single('image')  
    upload(req, res, function(err) {
        if (err) return res.send(err)
        const { role } = req.user
        if (role !== 'admin') {

        }else { 

            let productData = req.body
            let product = new Product(productData)
            product.image = req.file.path
            cloudinary.v2.uploader.upload(req.file.path, function(error, result) {
                if (error) return console.log(error)
                product.cloudImageUrl = result.secure_url
                product.save((err, prd) => {
                    if (err) return res.status(400).send('Something went wrong')
    
                    res.status(201).json({message: 'Product added',
                                    product: prd
                                    });
                });
            })
            
        }
    })    
}

const getAllProducts = (req, res) => {
    Product.find((err, products) => {
        if (err) return res.status(400).send('Products not found')
        return res.status(200).json(products)
    })
}

const getSingleProduct = (req,res) => {
    let { role } = req.user
    if (role !== 'admin') {

    } else {
        Product.findById(req.params._id, (err, prod) => {
            if (err) {
                return res.status(422).send('User not found')
            } else {
                return res.status(200).send(prod)
            }
        });
    }
}
const singleProductUpdate = (req, res) => {
    let id = req.params._id
    let updateData = req.body
    const { role } = req.user
    if (role !== 'admin'){
        return res.status(403).send('Forbidden')
    } else {
        Product.findByIdAndUpdate(id, {$set: updateData}, (err, doc) => {
            if (err) {
                return res.status(422).send(err)
            } else {
                return res.status(200).send(doc)
            }
        });
    }  
}

const removeProduct = (req, res) => {
    let id = req.params._id
    const { role } = req.user
    if (role === 'admin') {
        Product.findByIdAndDelete(id, (err, result) => {
            if (err) return res.status(400).send('Something Wrong')
            return res.status(200).send({'result': result, 'message': 'succesfully removed'})
        });
    };
}

const logout = (req, res) => {
    res.clearCookie('ntoken')
    res.status(200).send('Admin logged out')
}

const getAllOrders = (req, res) => {
    let { role } = req.user
    if (role == 'admin') {
        Order.find((err, orders) => {
            if (err) return res.status(400).send('Products not found')
            return res.status(200).json(orders)
        })
    }
}

let adminCont = {
    'adminLogin': adminLogin,
    'allUsers': allUsers,
    'oneUser': getOneUser, 
    'updateUser': updateOneUser,
    'addProduct': addProduct,
    'allProducts': getAllProducts,
    'getSingleProduct': getSingleProduct,
    'addCategory': addCategory,
    'singleProductUpdate': singleProductUpdate,
    'removeProduct': removeProduct,
    'logout': logout,
    'getAllOrders': getAllOrders
}
      


export default adminCont
