import mongoose from 'mongoose'

const { Schema } = mongoose

const productSchema = new Schema({
    name: String,
    price: String,
    image: {
        type: String,
        required: true
    },
    cloudImageUrl: {
        type: String
    },
    status: {type: String, enum:['out_of_stock', 'in_stock', 'running_low'], default: 'in_stock'},
    quantity: Number,
    created_at: {type: Date, default:Date.now}
})

const CategorySchema = new Schema({
    name: String
})

const orderSchema = new Schema({
    reference_number: {type: String, Unique: true},
    user_id: { type: String, Unique: true},
    items: [{
        item_id: {type: Schema.Types.ObjectId, ref: "Products"},
        item_quantity: {type: Number, default: 1},
        _id: false
    }],
    total: Number,    
})




export {productSchema, CategorySchema, orderSchema}