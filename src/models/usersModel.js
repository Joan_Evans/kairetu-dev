import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'
const {Schema} = mongoose

const userSchema = new Schema({
    name:String,
    email:String,
    password:String,
    created_at: {type: Date, default: Date.now},
    phonenumber: String,
    payment: {type: Schema.Types.ObjectId, ref: 'Payment'},
    role: {type: String, default: 'shopper'}
})

const paymentSchema = new Schema({
    paymentMethod:String,
    paymentDesc:String
})



userSchema.methods.matchPassword = function(password, callback) {
    return callback(null, bcrypt.compareSync(password, this.password))
}

let modelExports = {
    'userSchema': userSchema,
    'paymentSchema': paymentSchema,
}
export default modelExports